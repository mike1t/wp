<!doctype html>
<html lang="ru">
<head>
	<meta charset="UTF-8">
	<meta name="viewport"
	      content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
	<meta http-equiv="X-UA-Compatible" content="ie=edge">
	<title><?php wp_title( '«', true, 'right' ); ?><?php bloginfo( 'name' ); ?></title>
	<!-- Libs -->
	<link rel="stylesheet" href="/css/font-awesome.css">
	<link rel="stylesheet" type="text/css" href="/slick/slick.css">
	<link rel="stylesheet" type="text/css" href="/slick/slick-theme.css">
	<link href="https://fonts.googleapis.com/css?family=Nunito|Oswald|Saira+Extra+Condensed|Vollkorn"
	      rel="stylesheet">
	<!-- Стили -->
	<link rel="stylesheet" href="/css/style.css">
	<link rel="stylesheet" href="/css/contact.css">
	<link rel="stylesheet" href="/css/header.css">
	<link rel="stylesheet" href="/css/footer.css">
</head>
<body>

