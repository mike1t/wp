
<div class="wrapper">

    <!-- Место для банера-->
    <div class="wrap-banner">
        <div class="banner"></div>
    </div>


    <!-- Лого -->
    <div class="wrap-logo">
        <div class="logo">
            <a href="#">Pro-Obnal</a>
            <span>Возьми жизнь в свои руки</span>
        </div>
    </div>


    <!-- Меню -->
    <div class="wrap-menu">
        <div class="menu">
            <ul>
                <li class="current_page_item">
                    <a href="http://pro-obnal.ru/">
                        <span>Главная</span>
                    </a>
                </li>
                <li class="page_item page-item-153">
                    <a href="http://pro-obnal.ru/?page_id=153">
                        <span>Работа дропом</span>
                    </a>
                </li>
                </li>
                <li class="page_item page-item-5">
                    <a href="http://pro-obnal.ru/?page_id=5">
                        <span>Что такое кардинг?!</span>
                    </a>
                </li>
                <li class="page_item page-item-7">
                    <a href="http://pro-obnal.ru/?page_id=7">
                        <span>Заливы</span>
                    </a>
                </li>
                <li class="page_item page-item-9">
                    <a href="http://pro-obnal.ru/?page_id=9">
                        <span>Дамп+пин</span>
                    </a>
                </li>
                <li class="page_item page-item-13">
                    <a href="http://pro-obnal.ru/?page_id=13">
                        <span>Обучение кардингу</span>
                    </a>
                </li>
                <li class="page_item page-item-19">
                    <a href="http://pro-obnal.ru/?page_id=19">
                        <span>Продажа дебетовых карт</span>
                    </a>
                </li>
                <li class="page_item page-item-21">
                    <a href="http://pro-obnal.ru/?page_id=21">
                        <span>Связь с нами (Контакты)</span>
                    </a>
                </li>
                <li class="page_item page-item-23">
                    <a href="http://pro-obnal.ru/?page_id=23">
                        <span>Отзывы</span>
                    </a>
                </li>
                <li class="page_item page-item-48">
                    <a href="http://pro-obnal.ru/?page_id=48">
                        <span>Раздел для сотрудников ( Дропов )</span>
                    </a>
                </li>
            </ul>
        </div>
    </div>

    <!-- Слайдер -->
    <div class="wrap-slide">
        <div class="slide">
            <h1>Работа дропом без предоплаты</h1>
            <div class="sl">
                <div class="sl__slide"><img src="img/3.jpg" alt="img1" class="sl__img">
                    <div class="sl__text">
                        <h3 class="sl__zag">Работайте с нами и получайте больше</h3>
                        <p class="sl__desc">Хотим предложить вам простую работу за которую вы будете
                            получать примерно от 6000$ в месяц. Все что вам нужно будет делать так
                            это ходить и снимать деньги с банкоматов (согласитесь это даже под силу
                            школьнику).</p>
                    </div>
                </div>
                <div class="sl__slide"><img src="img/5.jpg" alt="img2" class="sl__img">
                    <div class="sl__text">
                        <h3 class="sl__zag">Хватит работать за копейки</h3>
                        <p class="sl__desc">Начните с нами уже сегодня!</p>
                    </div>
                </div>
                <div class="sl__slide">
                    <img src="img/6.jpg" alt="img3" class="sl__img">
                    <div class="sl__text">
                        <h3 class="sl__zag">Время деньги</h3>
                        <p class="sl__desc">Подумайте сколько вы тратите времени и сил на то чтоб
                            заработать 6000 $ на своей работе?! Сколько времени вы будете тратить
                            работая с нами? Максимум 10 часов в месяц!</p>
                    </div>
                </div>
                <div class="sl__slide"><img src="img/11.jpg" alt="img4" class="sl__img">
                    <div class="sl__text">
                        <h3 class="sl__zag">Деньги</h3>
                        <p class="sl__desc">&laquo;Деньги — это шестое чувство, которое позволяет
                            наслаждаться плодами первых пяти.&raquo; Уильям Сомерсет Моэм</p>
                    </div>
                </div>
                <div class="sl__slide"><img src="img/18.jpg" alt="img5" class="sl__img">
                    <div class="sl__text">
                        <h3 class="sl__zag">Живи</h3>
                        <p class="sl__desc">&laquo;Не будь расточителен, не будешь нуждаться.&raquo;
                            Вальтер Скотт</p>
                    </div>
                </div>
            </div>
        </div>
    </div>


    <!-- Возможности -->
    <div class="wrap_opportunity">
        <div class="opportunity">
            <div class="opportunity__one">
                <div class="opportunity__img1">
                    <div class="opportunity__ton">
                        <strong>Заливы</strong>
                    </div>
                </div>
                <p>Всю нужную информацию о заливах вы можете узнать здесь...</p>
                <a href="#">Читать далее &raquo; </a>
            </div>
            <div class="opportunity__two">
                <div class="opportunity__img2">
                    <div class="opportunity__ton">
                        <strong>Дамп+пин</strong>
                    </div>
                </div>
                <p>Что такое ДАМП + ПИН ? Как вы получаете ДАМПЫ + ПИН? читаем здесь...</p>
                <a href="#">Читать далее &raquo; </a>
            </div>
            <div class="opportunity__three">
                <div class="opportunity__img3">
                    <div class="opportunity__ton">
                        <strong>Схемы работы для дропов</strong>
                    </div>
                </div>
                <p>Как стать дропом, как начать работать с нами и на каких условиях вы узнаете
                    тут...</p>
                <a href="#">Читать далее &raquo; </a>
            </div>
        </div>
    </div>


    <!-- Информационные блоки-->
    <div class="wrap_info">

        <div class="info">
            <i class="fa fa-bullhorn" aria-hidden="true"></i>
            <div class="info__wrapContent">
                <p class="info__content">Здравствуйте, глубокоуважаемые посетители нашего сайта, а
                    также
                    уже наши потенциальные клиенты (дропы) нашего официального сайта. Вам надоело
                    подниматься каждый битый день в 6,7 утра или ещё раньше? Надоело зарабатывать
                    копейки за которые нормально прожить невозможно? Или же вы без работы вообще?!
                    Задайте себе вопрос: сколько я получаю и что мне доводится для этого делать?
                    Команда
                    "PRO-OBNAL" предлагает вам весьма простую работу " работа дропом ''. Что это
                    такое?
                    Сколько платят? Что нужно делать? Что такое dump+pin? Что такое залив? Что такое
                    белый пластик? Все ответы вы найдете здесь, на нашем сайте!</p>
            </div>
        </div>

        <div class="info">
            <i class="fa fa-users" aria-hidden="true"></i>
            <div class="info__wrapContent">
                <p class="info__content">Мы занимаемся кардингом (про это вы сможете почитать на
                    нашем
                    сайте) в разделе, что такое кард Инг?! », а вы, станете нашими дропами и будете
                    заниматься обналичиванием пластика (обналичивание кредитных карт, а точнее их
                    дубликатов). Вы получаете 5 карт (белый пластик уже залитый с балансом) с
                    балансом
                    не меньше 2000 долларов на каждой и у вас будет простая задача - обналичивать их
                    в
                    банкоматах (все очень просто и без рисков). Мы работаем 50 на 50 после
                    обналичивания
                    50 % наши и 50% ваши. Теперь давайте посчитаем ваши заработанные деньги за такую
                    работу 5 карт умножим минимум на 2000 долларов на каждой карте = 10 000 долларов
                    с
                    них 50% (5000 долларов ваши). Вы потратили максимум 10 часов работы за месяц и
                    ваш
                    доход 5000 долларов! А если провернуть такое 2 раза в месяц? Неплохо, да?
                    Знакомьтесь с сайтом и пишите нам для сотрудничества! Ждем всех!</p>
            </div>
        </div>

        <div class="info">
            <i class="fa fa-bookmark-o" aria-hidden="true"></i>
            <h1 class="info__head">Объявляем набор дропов</h1>
            <div class="info__wrapContent">
                <p class="info__content">Объявляем набор дропов на октябрь. Набор ограничен. (Нужны
                    5
                    дропов). PS: Инфо для дроповодов-москвичей: собрание будет 25 сентября в 19.00.
                    Место то же. Не опаздываем.</p>
            </div>
            <span class="info__qtyComment"><i class="fa fa-comments-o" aria-hidden="true"></i><a
                    href="#">&nbsp;9 комментариев</a></span>
            <div class="info__date">от ADMIN 22 Сентября 2017 Новости</div>
        </div>

        <div class="info">
            <i class="fa fa-bookmark-o" aria-hidden="true"></i>
            <h1 class="info__head">Ищем курьеров на закладки</h1>
            <div class="info__wrapContent">
                <p class="info__content">Все больше и больше постоянные клиенты заказывают у нас
                    анонимную доставку в разные города, когда они ездят по «гастролям». Требуются
                    люди,
                    способные грамотно сделать закладку карт. От вас нужен энкодер, либо можете
                    купить
                    его у нас. За одну закладку мы платим 2500 рублей вам на кошелек, удачный снал
                    всех
                    карт от …</p>
            </div>
            <span class="info__qtyComment"><i class="fa fa-comments-o" aria-hidden="true"></i><a
                    href="#">&nbsp;4 комментариев</a></span>
            <div class="info__date">от ADMIN 1 Июня 2017 Новости</div>
        </div>

        <div class="info">
            <i class="fa fa-bookmark-o" aria-hidden="true"></i>
            <h1 class="info__head">Условия по заливам</h1>
            <div class="info__wrapContent">
                <p class="info__content">Работаем 50% на 50% Минимальные заливы от 50 000 рублей
                    Сроки —
                    в течении 10 часов после оформления сделки через гаранта. Сразу напишу —
                    ПРЕДОПЛАТА
                    10% от суммы залива, которую вы хотите получить. Куда заливаем?: 1. Банковские
                    счета
                    РФ и Украины 2. Платежные системы: Яндекс Деньги, WebMoney, Qiwi, Payeer,
                    Skrill,
                    …</p>
            </div>
            <span class="info__qtyComment"><i class="fa fa-comments-o" aria-hidden="true"></i><a
                    href="#">&nbsp;1 комментариев</a></span>
            <div class="info__date"> ОТ ADMIN 22 Сентября 2017 Новости</div>
        </div>
        <div class="info">
            <i class="fa fa-bookmark-o" aria-hidden="true"></i>
            <h1 class="info__head">Объявляем набор дропов</h1>
            <div class="info__wrapContent">
                <p class="info__content">Объявляем набор дропов на октябрь. Набор ограничен. (Нужны
                    5
                    дропов). PS: Инфо для дроповодов-москвичей: собрание будет 25 сентября в 19.00.
                    Место то же. Не опаздываем.</p>
            </div>
            <span class="info__qtyComment"><i class="fa fa-comments-o" aria-hidden="true"></i><a
                    href="#">&nbsp;9 комментариев</a></span>
            <div class="info__date">от ADMIN 22 Сентября 2017 Новости</div>
        </div>

        <div class="info">
            <i class="fa fa-bookmark-o" aria-hidden="true"></i>
            <h1 class="info__head">Ищем курьеров на закладки</h1>
            <div class="info__wrapContent">
                <p class="info__content">Все больше и больше постоянные клиенты заказывают у нас
                    анонимную доставку в разные города, когда они ездят по «гастролям». Требуются
                    люди,
                    способные грамотно сделать закладку карт. От вас нужен энкодер, либо можете
                    купить
                    его у нас. За одну закладку мы платим 2500 рублей вам на кошелек, удачный снал
                    всех
                    карт от …</p>
            </div>
            <span class="info__qtyComment"><i class="fa fa-comments-o" aria-hidden="true"></i><a
                    href="#">&nbsp;4 комментариев</a></span>
            <div class="info__date">от ADMIN 1 Июня 2017 Новости</div>
        </div>

        <div class="info">
            <i class="fa fa-bookmark-o" aria-hidden="true"></i>
            <h1 class="info__head">Условия по заливам</h1>
            <div class="info__wrapContent">
                <p class="info__content">Работаем 50% на 50% Минимальные заливы от 50 000 рублей
                    Сроки —
                    в течении 10 часов после оформления сделки через гаранта. Сразу напишу —
                    ПРЕДОПЛАТА
                    10% от суммы залива, которую вы хотите получить. Куда заливаем?: 1. Банковские
                    счета
                    РФ и Украины 2. Платежные системы: Яндекс Деньги, WebMoney, Qiwi, Payeer,
                    Skrill,
                    …</p>
            </div>
            <span class="info__qtyComment"><i class="fa fa-comments-o" aria-hidden="true"></i>
                <a href="#">&nbsp;1 комментариев</a></span>
            <div class="info__date"> ОТ ADMIN 22 Сентября 2017 Новости</div>
        </div>
    </div>


    <!-- footer -->
    <div class="wrap-footer">
        <footer>
            <div class="counters1"></div>
            <div class="counters2"></div>
            <div class="counters3"></div>
            <h5 class="copyrighted">
                <a href="#">PRO-obnal.ru&nbsp;
                    <i class="fa fa-registered" aria-hidden="true"></i>
                </a>&nbsp;&nbsp;&nbsp;&nbsp;2008-2018
            </h5>
        </footer>

        <a href="#">
            <i class="fa fa-angle-up" aria-hidden="true"></i>
        </a>

    </div>

</div>

