<?php
	function wp_obnal_script() {
		wp_enqueue_style( 'obnal', get_stylesheet_uri() );



		if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
			wp_enqueue_script( 'comment-reply' );
		}



/*
		wp_enqueue_script( 'scrollto',
			get_template_directory_uri() . '/libs/balupton-jquery-scrollto-a1eb8a5/lib/jquery-scrollto.js',
			[ 'jquery' ], '', true );

		wp_enqueue_script( 'jquery-spincrement',
			get_template_directory_uri() . '/libs/jquery-spincrement-master/jquery.spincrement.js', [ 'jquery' ], '',
			true );

		wp_enqueue_script( 'jquery-viewport-checker',
			get_template_directory_uri() . '/libs/jQuery-viewport-checker/dist/jquery.viewportchecker.min.js',
			[ 'jquery' ], '', true );*/



		wp_enqueue_script( 'modernizr', get_template_directory_uri() . '/libs/modernizr/modernizr.js', [ 'jquery' ], '',
			false );

		wp_enqueue_script( 'respond', get_template_directory_uri() . '/libs/html5shiv/respond.min.js' , array('jquery'), '', false);

		wp_enqueue_script( 'html5shiv', get_template_directory_uri() . '/libs/html5shiv/html5shiv.min.js' , array('jquery'), '', false);


	}

	add_action( 'wp_enqueue_scripts', 'wp_obnal_script' );