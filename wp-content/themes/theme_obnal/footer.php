<script type="text/javascript"
        src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<script type="text/javascript" src="/slick/slick.min.js"></script>
<script src="/js/js.js" defer></script>

<!-- footer -->
<div class="wrap-footer">
	<footer>
		<div class="counters1"></div>
		<div class="counters2"></div>
		<div class="counters3"></div>
		<h5 class="copyrighted">
			<a href="#">PRO-obnal.ru&nbsp;
				<i class="fa fa-registered" aria-hidden="true"></i>
			</a>&nbsp;&nbsp;&nbsp;&nbsp;2008-2018
		</h5>
	</footer>

	<a href="#">
		<i class="fa fa-angle-up" aria-hidden="true"></i>
	</a>

</div>

</div>
</body>
</html>