/**
 * Created by Marcang on 27.05.2017.
 */
jQuery(document).ready(function($) {
    $("#contact").submit(function() {
        var str = $(this).serialize();
        $.ajax({
            type: "POST",
            url: "/wp-content/themes/fight/mail.php",
            data: str,
            success: function(msg) {
                if(msg == 'OK') {
                    result = '<div class="ok">Сообщение отправлено</div>';
                    $("#fields").hide();
                }
                else {result = msg;}
                $('#note').html(result);
            }
        });
        return false;
    });
});