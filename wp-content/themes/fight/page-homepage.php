<?
/**
 * The template for displaying all pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package fight
 **/

get_header(); ?>

	<section class="aboutus">
		<div class="container second">
			<div class="circle__gradient">
			</div>
			<div class="row">
				<div class="col-md-5">
					<div class="about__text">
						<h1>
							<?php
							echo get_field( 'about-us' );
							?>
						</h1>
						<p><?php
							echo get_field( 'text-about-us' );
							?>
						</p>
					</div>
				</div>
				<div class="col-md-7"></div>
			</div>
		</div>
		<div class="container-fluid bg-gloves">
			<div class="row">
				<div class="col-md-6">

				</div>
				<div class="col-md-offset-6 col-md-7">

					<div class="box__gloves img-responsive">
					</div>
				</div>
			</div>
			<!--	<div class="row">
					<div class="#addform">
						<div class="form_feedback">
							<h1>Оставьте нам сообщение и мы вам перезвоним</h1>
							<form id="contact" action="/wp-content/themes/fight/mail.php" method="post">
								<div id="note"></div>
								<div id="fields"><p>
										<label for="name1" class="name pull-left">Имя</label>
										<input type="text" id="name1" name="name" class="pull-right"
											   placeholder="(обязательно)" required/>
									</p>
									<p>
										<label for="company1" class="company pull-left">Компания</label>
										<input type="text" id="company1" class="pull-right"
											   placeholder='(не обязательно)' name="company"/></p>
									<p><label for="telephone1" class="telephone pull-left">Телефон</label>
										<input type="text" name="tel" id="telephone1" class="pull-right"
											   placeholder="(обязательно)"
											   required/></p>
									<p><label for="message1" class="message pull-left">Cообщение</label>
									<textarea cols="30" name="message" id="message1"
											  placeholder="(не обязательно)"></textarea></p>
									<p><label for="submit1" class="feedback__txt pull-left"><span>*</span> мы Вам
											перезвоним</label>
										<input id="submit1" type="submit" class="feedback__submit pull-right"
											   value="Отправить">
									</p>
								</div>
							</form>
						</div>
					</div>
				</div>-->
		</div>
	</section>
	<section class="add-form animated lightSpeedIn">
		<div class="container">
			<div class="row">
				<div class="col-xs-12 animated lightSpeedIn">
					<div class="form_feedback animated lightSpeedIn" id="addform">
						<h1>Оставьте нам сообщение и мы вам перезвоним</h1>
						<form id="contact" action="/wp-content/themes/fight/mail.php" method="post">
							<div id="note"></div>
							<div id="fields"><p>
									<label for="name1" class="name pull-left">Имя</label>
									<input type="text" id="name1" name="name" class="pull-right"
									       placeholder="(обязательно)" required/>
								</p>
								<p>
									<label for="company1" class="company pull-left">Компания</label>
									<input type="text" id="company1" class="pull-right"
									       placeholder='(не обязательно)' name="company"/></p>
								<p><label for="telephone1" class="telephone pull-left">Телефон</label>
									<input type="text" name="tel" id="telephone1" class="pull-right"
									       placeholder="(обязательно)"
									       required/></p>
								<p><label for="message1" class="message pull-left">Cообщение</label>
								<textarea cols="30" name="message" id="message1"
								          placeholder="(не обязательно)"></textarea></p>
								<p><label for="submit1" class="feedback__txt pull-left"><span>*</span> мы Вам
										перезвоним</label>
									<input id="submit1" type="submit" class="feedback__submit pull-right"
									       value="Отправить">
								</p>
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>
	</section>
	<section class="fights" id="fights">
		<div class="container">
			<div class="row ">
				<div class="col-md-4 col-xs-12 label1">
					<h2>
						<a href="#">Показать все фото</a>
					</h2>
				</div>
				<div class="col-md-4 col-xs-12 label2">
					<h2>
						info@veglivie.ru
					</h2>
				</div>
				<div class="col-md-4 col-xs-12 label3">
					<h2>
						<?php /*get_search_form(); */?>8 495 642 51 45
					</h2>
				</div>
			</div>
		</div>

		<div class="container fight_cards">
			<div class="row">
				<div class="col-md-4 col-xs-12">
					<a href="#" class="card1">
						<div class="card-1">
							<div class="top">
								<img src="/wp-content/uploads/2017/06/v-fitka5.jpg" alt="" class="img-responsive center-block">
							</div>
							<div class="bottom">
								<h2>Спаринг - тайский бокс</h2>
								<p>Запечатлен момент перед ударом коленом, второй боксер готов и ставит блок</p>
								<p class="clock__text">20 марта 2016</p>
								<p class="eye__text">140</p>
							</div>
						</div>
					</a>
				</div>
				<div class="col-md-4 col-xs-12">
					<a href="#" class="card1">
						<div class="card-1">
							<div class="top">
								<img src="/wp-content/uploads/2017/06/v-fitka11.jpg" alt="" class="img-responsive center-block">
							</div>
							<div class="bottom">
								<h2>2017.04.23 "Moscow Open" СБИ ММА</h2>
								<p>"Вежливые Люди" представляли: Хасанов Тимур - 1 место, Лапта Роман - 1 место, Жданов Данила - 2 место</p>
								<p class="clock__text">23 марта 2017</p>
								<p class="eye__text">740</p>
							</div>
						</div>
					</a>
				</div>
				<div class="col-md-4 col-xs-12">
					<a href="#" class="card2">
						<div class="card-1">
							<div class="top">
								<img src="/wp-content/uploads/2017/06/v-fitka7.jpg" alt="" class="img-responsive center-block">
							</div>
							<div class="bottom">
								<h2>Спаринг взрослых бойцов</h2>
								<p>На данном снимке партер - и рефери дает брейк</p>
								<p class="clock__text">26 апреля 2017</p>
								<p class="eye__text">125</p>
							</div>
						</div>
					</a>
				</div>
			</div>   <!-- 1 line -->
			<!--		</div>-->
			<!--		<div class="container fight_cards line2">-->
			<div class="row line2">
				<div class="col-md-4 col-xs-12">
					<a href="#" class="card3">
						<div class="">
							<div class="top">
								<img src="/wp-content/uploads/2017/06/v-fitka6.jpg" alt="" class="img-responsive center-block">
							</div>
							<div class="bottom">
								<h2>2017.04.23 "Moscow Open" СБИ ММА</h2>
								<p>Детский состав "Вежливые Люди" проявил себя на данном турнире на отлично</p>
								<p class="clock__text">23 марта 2017</p>
								<p class="eye__text">320</p>
							</div>
						</div>
					</a>
				</div>
				<div class="col-md-8 col-xs-12">
					<a href="#" class="card-wide">
						<div class="">
							<div class="top">
								<img src="http://fight-cms/wp-content/uploads/2017/06/v-fitka10.jpg" alt=""
								     class="img-responsive">
							</div>
							<div class="bottom">
								<h2>Снимок части команды "Вежливых Людей"</h2>
								<p>Взрослые, дети все вместе - одна сплоченная команда!!! Взрослые подают пример детям, развивая силу духа и упорство. "Никогда не опускай руки!"</p>
								<p class="clock__text">1 мая 2017</p>
								<p class="eye__text">320</p>
							</div>
						</div>
					</a>
				</div>
			</div> <!--end line 2-->
			<div class="row line3">
				<div class="col-md-4 col-xs-12">
					<a href="#" class="card1">
						<div class="">
							<div class="top">
								<img src="/wp-content/uploads/2017/06/v-fitka4.jpg" alt="" class="img-responsive center-block">
							</div>
							<div class="bottom">
								<h2>Получение награды</h2>
								<p>Да, да - вы не ослышались, у нас занимаются даже девченки! и при этом показывают как видите результаты не хуже ребят. </p>
								<p class="clock__text">1 июня 2017</p>
								<p class="eye__text">320</p>
							</div>
						</div>
					</a>
				</div>
				<div class="col-md-4 col-xs-12">
					<a href="#" class="card-image-text">
						<div class="card-all">
							<p class="anytext">Девиз и призыв</p>
							<img src="/wp-content/themes/fight/img/cards/card7-content-box-gloves.png" alt=""
							     class="img-responsive img-card center-block">
							<h2 class="overlay__text">Вступай к нам в семью и помни никогда не опускай руки! Все получится.</h2>
							<p class="clock__text">1 июня 2017</p>
							<p class="eye__text">320</p>
						</div>
					</a>
				</div>
				<div class="col-md-4 col-xs-12">
					<a href="#" class="card1">
						<div class="">
							<div class="top">
								<img src="/wp-content/uploads/2017/06/v-fitka8.jpg" alt=""
								     class="img-responsive">
							</div>
							<div class="bottom">
								<h2>Партер - бой окончен</h2>
								<p>Детский спаринг один из бойцов одерживает победу в партере</p>
								<p class="clock__text">20 мая 2017</p>
								<p class="eye__text">317</p>
							</div>
						</div>
					</a>
				</div>
			</div>
		</div>
	</section>
	<div></div>
	<section class="our_treners">
		<div class="container">
			<div class="row">
				<div class="col-xs-12 col-md-12">
					<h1>НАШИ ТРЕНЕРЫ</h1>
				</div>
			</div>
			<div class="row">
				<div class="col-md-4 col-xs-12">
					<div class="wrapper">
						<img class="img-rounded center-block img-responsive"
						     src="/wp-content/uploads/2017/06/v-fitka12.jpg" alt="">
					</div>
				</div>
				<div class="col-md-4 col-xs-12">
					<div class="desc white img-rounded center-block">
						<h2>Куприянов О. В.</h2>
						<p class="desc__spec">Эксперт по смешанным боевым единоборствам</p>
						<p class="desc__txt">
							Мастер спорта СССР по Дзю-до, каратэ-до киокусинкай 3 дан, джиу-джитсу 3 дан, айкидо 3 дан, кендо 3 дан, кру (мастер) Муай Тай, инструктор по тактической стрельбе и ножевому бою. Основатель Академии Муай Тай и Всемирной Бойцовской Лиги
						</p>
					</div>
				</div>
				<div class="col-md-4 col-xs-12">
					<div class="wrapper">
						<img class="img-rounded center-block img-responsive"
						     src="/wp-content/uploads/2017/06/v-fitka14.jpg" alt="">
					</div>
				</div>
					<!--<div class="col-md-3 col-xs-12">
						<div class="desc red img-rounded center-block">
							<h2></h2>
							<p class="desc__spec"></p>
							<p class="desc__txt"></p>
							<p class="desc__mail"></p>
						</div>
					</div>-->
			</div>
			<div class="row">
				<div class="col-md-4 col-xs-12">

					<div class="desc grey img-rounded center-block">
						<h2>Мельниченко А.С.</h2>
						<p class="desc__spec">Тайский бокс</p>
						<p class="desc__txt text-justify">
							Кандидат в мастера спортапо боксу, Мастер спорта по тайскому боксу,Тренерский стаж 4 года. Хорошее знание спортивной диетологии, работа с травмированными спортсменам.
						</p>
					</div>
				</div>
				<div class="col-md-4 col-xs-12">
					<div class="wrapper">
						<img class="img-rounded center-block img-responsive"
						     src="/wp-content/uploads/2017/06/v-fitka13.jpg" alt="">
					</div>
				</div>
				<div class="col-md-4 col-xs-12">
					<div class="desc dark img-rounded center-block">
						<h2>Афонин Я. В.</h2>
						<p class="desc__spec">Тренер по миксфайту</p>
						<p class="desc__txt">Победитель и призер турниров по смешенным единоборства различного уровня. Свой путь начал с 2000 го года. Его ученики регулярно становятся призерами в турнирах по смешенным единоборствам и грэпплингу различного уровня. Тренерский стаж 6 лет.
						</p>
					</div>
				</div>
<!--				<div class="col-md-4 col-xs-12   ln2">
					<div class="wrapper">-->
							<!--<img class="img-rounded center-block"
							     src="/wp-content/themes/fight/img/our_treners/fedor_em.png" alt="">-->
<!--					</div>
				</div>-->
			</div>
		</div>
	</section>
	<section class="plan">
		<div class="circle__gradient2">
		</div>
		<div class="container">
			<div class="row">
				<div class="col-md-12"><h1 class="head__txt">Расписание</h1></div>
			</div>
		</div>

		<div class="container">
			<div class="plan">
				<div class="col-md-6 col-xs-12">
					<div class="plann">
						<div class="plann__left"><p>
								<?php
								echo get_field( 'plan-date' );
								?></p>
						</div>
						&nbsp;&nbsp;&nbsp;&nbsp;
						<div class="plann__right"><p><?php
								echo get_field( 'plan-date-1-trener' );
								?></p></div>

						<p class="plann__txt">
							<?php
							echo get_field( 'plan-date-txt-1' );
							?>
						</p>
					</div>
					<div class="plann">
						<div class="plann__left"><p><?php
								echo get_field( 'plan-date-2' );
								?></p></div>
						&nbsp;&nbsp;&nbsp;&nbsp;
						<div class="plann__right"><p><?php
								echo get_field( 'plan-date-2-trener' );
								?></p></div>

						<p class="plann__txt">
							<?php
							echo get_field( 'plan-date-txt-2' );
							?>
						</p>
					</div>
					<div class="plann">
						<div class="plann__left"><p><?php
								echo get_field( 'plan-date-3' );
								?></p></div>
						&nbsp;&nbsp;&nbsp;&nbsp;
						<div class="plann__right"><p><?php
								echo get_field( 'plan-date-3-trener' );
								?></p></div>

						<p class="plann__txt">
							<?php
							echo get_field( 'plan-date-txt-3' );
							?>
						</p>
					</div>
					<div class="plann">
						<div class="plann__left"><p><?php
								echo get_field( 'plan-date-4' );
								?></p></div>
						&nbsp;&nbsp;&nbsp;&nbsp;
						<div class="plann__right"><p><?php
								echo get_field( 'plan-date-4-trener' );
								?></p></div>

						<p class="plann__txt">
							<?php
							echo get_field( 'plan-date-txt-4' );
							?>
						</p>
					</div><div class="plann">
						<div class="plann__left"><p><?php
								echo get_field( 'plan-date-5' );
								?></p></div>
						&nbsp;&nbsp;&nbsp;&nbsp;
						<div class="plann__right"><p><?php
								echo get_field( 'plan-date-5-trener' );
								?></p></div>

						<p class="plann__txt">
							<?php
							echo get_field( 'plan-date-txt-5' );
							?>
						</p>
					</div><div class="plann">
						<div class="plann__left"><p><?php
								echo get_field( 'plan-date-6' );
								?></p></div>
						&nbsp;&nbsp;&nbsp;&nbsp;
						<div class="plann__right"><p><?php
								echo get_field( 'plan-date-6-trener' );
								?></p></div>

						<p class="plann__txt">
							<?php
							echo get_field( 'plan-date-txt-6' );
							?>
						</p>
					</div><div class="plann">
						<div class="plann__left"><p><?php
								echo get_field( 'plan-date-7' );
								?></p></div>
						&nbsp;&nbsp;&nbsp;&nbsp;
						<div class="plann__right"><p><?php
								echo get_field( 'plan-date-7-trener' );
								?></p></div>

						<p class="plann__txt">
							<?php
							echo get_field( 'plan-date-txt-7' );
							?>
						</p>
					</div><div class="plann">
						<div class="plann__left"><p><?php
								echo get_field( 'plan-date-8' );
								?></p></div>
						&nbsp;&nbsp;&nbsp;&nbsp;
						<div class="plann__right"><p><?php
								echo get_field( 'plan-date-8-trener' );
								?></p></div>

						<p class="plann__txt">
							<?php
							echo get_field( 'plan-date-txt-8' );
							?>
						</p>
					</div><div class="plann">
						<div class="plann__left"><p><?php
								echo get_field( 'plan-date-9' );
								?></p></div>
						&nbsp;&nbsp;&nbsp;&nbsp;
						<div class="plann__right"><p><?php
								echo get_field( 'plan-date-9-trener' );
								?></p></div>

						<p class="plann__txt">
							<?php
							echo get_field( 'plan-date-txt-9' );
							?>
						</p>
					</div>
				</div>
			</div>
			<div class="col-md-6 col-xs-12 hidden-xs hidden-sm">
				<div class="punch img-responsive">
				</div>
				<div class="shadow_bottom">

				</div>
			</div>
		</div>
		</div>
	</section>
	<section class="feedback">
		<div class="container feed">
			<div class="row">
				<div class="col-md-6 col-xs-12">
					<p class="head__txt">
						Вступай в нашу <br> семью
					</p>
					<div class="contacts">
						<div class="contacts__tel">+7 (495) 221-21-21</div>
						<div class="contacts__mail">info@veglivie.ru</div>
					</div>
				</div>
				<div class="col-md-6 col-xs-12">
					<div class="form_feedback">
						<form id="contact" action="/wp-content/themes/fight/mail.php" method="post">
							<div id="note"></div>
							<div id="fields"><p>
									<label for="name1" class="name pull-left">Имя</label>
									<input type="text" id="name1" name="name" class="pull-right"
									       placeholder="(обязательно)" required/>
								</p>
								<p>
									<label for="company1" class="company pull-left">Компания</label>
									<input type="text" id="company1" class="pull-right"
									       placeholder='(не обязательно)' name="company"/></p>
								<p><label for="telephone1" class="telephone pull-left">Телефон</label>
									<input type="text" name="tel" id="telephone1" class="pull-right"
									       placeholder="(обязательно)"
									       required/></p>
								<p><label for="message1" class="message pull-left">Cообщение</label>
								<textarea cols="30" name="message" id="message1"
								          placeholder="(не обязательно)"></textarea></p>
								<p><label for="submit1" class="feedback__txt pull-left"><span>*</span> мы Вам
										перезвоним</label>
									<input id="submit1" type="submit" class="feedback__submit pull-right"
									       value="Отправить">
								</p>
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>
	</section>
	<div id="primary" class="content-area">
		<main id="main" class="site-main" role="main">
		</main><!-- #main -->
	</div><!-- #primary -->
<?php
get_footer();
