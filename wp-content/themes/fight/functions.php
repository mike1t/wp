<?php
/**
 * fight functions and definitions
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package fight
 */

if ( ! function_exists( 'fight_setup' ) ) :
	/**
	 * Sets up theme defaults and registers support for various WordPress features.
	 *
	 * Note that this function is hooked into the after_setup_theme hook, which
	 * runs before the init hook. The init hook is too late for some features, such
	 * as indicating support for post thumbnails.
	 */
	function fight_setup() {
		/*
		 * Make theme available for translation.
		 * Translations can be filed in the /languages/ directory.
		 * If you're building a theme based on fight, use a find and replace
		 * to change 'fight' to the name of your theme in all the template files.
		 */
		load_theme_textdomain( 'fight', get_template_directory() . '/languages' );

		// Add default posts and comments RSS feed links to head.
		add_theme_support( 'automatic-feed-links' );

		/*
		 * Let WordPress manage the document title.
		 * By adding theme support, we declare that this theme does not use a
		 * hard-coded <title> tag in the document head, and expect WordPress to
		 * provide it for us.
		 */
		add_theme_support( 'title-tag' );

		/*
		 * Enable support for Post Thumbnails on posts and pages.
		 *
		 * @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
		 */
		add_theme_support( 'post-thumbnails' );

		// This theme uses wp_nav_menu() in one location.
		register_nav_menus( array(
			'menu-1' => esc_html__( 'Primary', 'fight' ),
		) );

		/*
		 * Switch default core markup for search form, comment form, and comments
		 * to output valid HTML5.
		 */
		add_theme_support( 'html5', array(
			'search-form',
			'comment-form',
			'comment-list',
			'gallery',
			'caption',
		) );

		// Set up the WordPress core custom background feature.
		add_theme_support( 'custom-background', apply_filters( 'fight_custom_background_args', array(
			'default-color' => 'ffffff',
			'default-image' => '',
		) ) );

		// Add theme support for selective refresh for widgets.
		add_theme_support( 'customize-selective-refresh-widgets' );
	}
endif;
add_action( 'after_setup_theme', 'fight_setup' );

/**
 * Set the content width in pixels, based on the theme's design and stylesheet.
 *
 * Priority 0 to make it available to lower priority callbacks.
 *
 * @global int $content_width
 */
function fight_content_width() {
	$GLOBALS['content_width'] = apply_filters( 'fight_content_width', 640 );
}

add_action( 'after_setup_theme', 'fight_content_width', 0 );

/**
 * Register widget area.
 *
 * @link https://developer.wordpress.org/themes/functionality/sidebars/#registering-a-sidebar
 */
function fight_widgets_init() {
	register_sidebar( array(
		'name'          => esc_html__( 'Sidebar', 'fight' ),
		'id'            => 'sidebar-1',
		'description'   => esc_html__( 'Add widgets here.', 'fight' ),
		'before_widget' => '<section id="%1$s" class="widget %2$s">',
		'after_widget'  => '</section>',
		'before_title'  => '<h2 class="widget-title">',
		'after_title'   => '</h2>',
	) );
}

add_action( 'widgets_init', 'fight_widgets_init' );

/**
 * Enqueue scripts and styles.
 */

add_action( 'wp_enqueue_scripts', 'my_scripts_method_jquery', 99 );
function my_scripts_method_jquery() {
	// получаем версию jQuery
	wp_enqueue_script( 'jquery' );
	// для версий WP меньше 3.6 'jquery' нужно поменять на 'jquery-core'
	$wp_jquery_ver = $GLOBALS['wp_scripts']->registered['jquery']->ver;
	$jquery_ver    = $wp_jquery_ver == '' ? '1.11.0' : $wp_jquery_ver;

	wp_deregister_script( 'jquery' );
	wp_register_script( 'jquery', '//ajax.googleapis.com/ajax/libs/jquery/' . $jquery_ver . '/jquery.min.js' );
	wp_enqueue_script( 'jquery' );
}

add_action( 'wp_enqueue_scripts', 'my_fancybox_scripts_method' );
function my_fancybox_scripts_method() {

}

function fight_scripts() {
	wp_enqueue_style( 'fight-style', get_stylesheet_uri() );

	wp_enqueue_script( 'fight-navigation', get_template_directory_uri() . '/js/navigation.js', array(), '20151215', true );

	wp_enqueue_script( 'fight-skip-link-focus-fix', get_template_directory_uri() . '/js/skip-link-focus-fix.js', array(), '20151215', true );

	if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
		wp_enqueue_script( 'comment-reply' );
	}

	if ( ! is_admin() ) {
		wp_enqueue_style( 'bootstrap-css', get_template_directory_uri() . '/css/bootstrap.min.css' );
	}
	wp_enqueue_script( 'bootstrap-js', get_template_directory_uri() . '/js/bootstrap.min.js', array( 'jquery' ), '', true );

	wp_enqueue_script( 'bootstrap-toolkit', get_template_directory_uri() . '/libs/bootstrap-toolkit/bootstrap-toolkit.min.js', array( 'jquery' ), '', true );

	wp_enqueue_script( 'main', get_template_directory_uri() . '/js/common.js', array( 'jquery' ), '1.0.2', true );

	wp_enqueue_script( 'wow', get_template_directory_uri() . '/libs/wow-master/dist/wow.min.js', array( 'jquery' ), '1.0.0', true );

	wp_enqueue_style( 'main-style', get_template_directory_uri() . '/css/main.css', array(), '', $media = 'all' );


	/*  fancybox */
	/*	wp_enqueue_style( 'fancybox', get_template_directory_uri() . '/libs/fancyboxx/jquery/fancybox.css');
		wp_enqueue_script( 'fancybox', get_template_directory_uri() . '/libs/fancyboxx/jquery.fancybox.js', array('jquery'), '', true);
		wp_enqueue_script( 'fancybox', get_template_directory_uri() . '/libs/fancyboxx/jquery.fancybox.pack.js', array('jquery'), '', true);

		wp_enqueue_style( 'fancybox', get_template_directory_uri() . '/libs/fancyboxx/jquery/fancybox.css');
		wp_enqueue_script( 'fancybox', get_template_directory_uri() . '/libs/fancyboxx/jquery.fancybox.js', array('jquery'), '', true);
		wp_enqueue_script( 'fancybox', get_template_directory_uri() . '/libs/fancyboxx/jquery.fancybox.pack.js', array('jquery'), '', true);*/

	wp_enqueue_script( 'scrollto', get_template_directory_uri() . '/libs/balupton-jquery-scrollto-a1eb8a5/lib/jquery-scrollto.js', array( 'jquery' ), '', true );

	wp_enqueue_script( 'jquery-spincrement', get_template_directory_uri() . '/libs/jquery-spincrement-master/jquery.spincrement.js', array( 'jquery' ), '', true );

	wp_enqueue_script( 'jquery-viewport-checker', get_template_directory_uri() . '/libs/jQuery-viewport-checker/dist/jquery.viewportchecker.min.js', array( 'jquery' ), '', true );


	wp_enqueue_script( 'jquery-animateNumber', get_template_directory_uri() . '/libs/jquery-animateNumber/jquery.animateNumber.min.js', array( 'jquery' ), '', true );

	wp_enqueue_script( 'modernizr', get_template_directory_uri() . '/libs/modernizr/modernizr.js', array( 'jquery' ), '', false );


	/*    wp_enqueue_script( 'respond', get_template_directory_uri() . '/libs/html5shiv/html5shiv.min.js' , array('jquery'), '', false);

		wp_enqueue_script( 'html5shiv', get_template_directory_uri() . '/libs/html5shiv/html5shiv.min.js' , array('jquery'), '', false);*/

	/*public
	function __construct( $template = 'base.php' ) {
		$this->slug      = basename( $template, '.php' );
		$this->templates = array( $template );

		if ( self::$base ) {
			$str = substr( $template, 0, - 4 );
			array_unshift( $this->templates, sprintf( $str . '-%s.php', self::$base ) );
		}
	}*/
	/*
	*/
	wp_enqueue_style( 'animate', get_template_directory_uri() . '/libs/animate.css/animate.min.css', array(), '', $media = 'all' );
}

add_action( 'wp_enqueue_scripts', 'fight_scripts' );

/**
 * Implement the Custom Header feature.
 */
require get_template_directory() . '/inc/custom-header.php';

/**
 * Custom template tags for this theme.
 */
require get_template_directory() . '/inc/template-tags.php';

/**
 * Custom functions that act independently of the theme templates.
 */
require get_template_directory() . '/inc/extras.php';

/**
 * Customizer additions.
 */
require get_template_directory() . '/inc/customizer.php';

/**
 * Load Jetpack compatibility file.
 */
require get_template_directory() . '/inc/jetpack.php';

/**
 * добавлюя форму поиска
 */
function my_search_form( $form ) {

	$form = '
	<form role="search" method="get" id="searchform" action="' . home_url( '/' ) . '" >
		<label class="screen-reader-text" for="s">Запрос для поиска:</label>
		<input type="text" value="' . get_search_query() . '" name="s" id="s" placeholder="Найти" />
		<input type="submit" id="searchsubmit" value="" />
	</form>';

	return $form;
}

add_filter( 'get_search_form', 'my_search_form' );
