<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package fightv2
 */
?>

<!--	</div>-->
<!-- #content -->
</div><!-- #page -->
<script src="/wp-content/themes/fight/js/contact.js"></script>
<?php wp_footer(); ?>
</body>
</html>
