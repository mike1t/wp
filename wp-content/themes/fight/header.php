<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package fight
 */

?><!DOCTYPE html>
<html lang="en">
<head>
	<meta http-equiv="CONTENT-TYPE" content="text/html" charset="UTF-8">
	<title><?php wp_title( '«', true, 'right' ); ?><?php bloginfo( 'name' ); ?></title>
	<meta name="description" content="">
	<meta name="keyword" content="">

	<meta http-equiv="x-ua-compatible" content="IE=edge, chrome=1">
	<meta name="viewport" content="width=device-width">
	<meta property="og:image" content="path/to/image.jpg">
	<link rel="shortcut icon" href="img/favicon/favicon.ico" type="image/x-icon">
	<link rel="apple-touch-icon" href="img/favicon/apple-touch-icon.png">
	<link rel="apple-touch-icon" sizes="72x72" href="img/favicon/apple-touch-icon-72x72.png">
	<link rel="apple-touch-icon" sizes="114x114" href="img/favicon/apple-touch-icon-114x114.png">
	<!-- Chrome, Firefox OS and Opera -->
	<meta name="theme-color" content="#000">
	<!-- Windows Phone -->
	<meta name="msapplication-navbutton-color" content="#000">
	<!-- iOS Safari -->
	<meta name="apple-mobile-web-app-status-bar-style" content="#000">

	<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
<div id="page" class="site">
	<!--	<a class="skip-link screen-reader-text" href="#content">-->
	<?php //esc_html_e( 'Skip to content', 'fight' ); ?><!--</a>-->
	<header id="masthead" class="site-header" role="banner">
		<div class="top_container">
			<div class="container">
				<div class="row">
					<div class="head__menu">
						<div class="col-md-5 col-xs-5">
							<ul class="mnu">
								<li class="mnu__item active">
									<a href="#" id="m1" class="mnu__link">Расписание</a>
								</li>
								<li class="mnu__item">
									<a href="#" id="m2" class="mnu__link">Блог</a>
								</li>
								<li class="mnu__item">
									<a href="#" id="m3" class="mnu__link">О нас</a>
								</li>
								<li class="mnu__item">
									<a href="#" id="m4" class="mnu__link">Контакты</a>
								</li>
							</ul>
						</div>
						<div class="col-lg-7 col-md-7 col-xs-7">
							<span class="main__contacts pull-right">8 495 642 51 45</span>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="container-fluid">
			<div class="row">
				<div class="col-md-12 col-xs-12 main_bg">
					<div id="logo">
						<p class="inner__text">
							Академия единоборств, в которой приобщают мужчин к
							радостям физической жестокости и силе духа.
						</p>
					</div>
				</div>
			</div>
		</div>

		<div class="site-branding">
<!--			<?php
/*			if ( is_front_page() && is_home() ) : */?>
				<h1 class="site-title"><a href="<?php echo esc_url( home_url( '/' ) ); ?>"
				                          rel="home"><?php /*bloginfo( 'name' ); */?></a></h1>
			<?php /*else : */?>
				<p class="site-title"><a href="<?php /*echo esc_url( home_url( '/' ) ); */?>"
				                         rel="home"><?php /*bloginfo( 'name' ); */?></a></p>
				<?php
/*			endif;
			$description = get_bloginfo( 'description', 'display' );
			if ( $description || is_customize_preview() ) : */?>
				<p class="site-description"><?php /*echo $description; /* WPCS: xss ok. */ ?></p>
				--><?php
/*			endif; */?>
		</div><!-- .site-branding -->

		<!--		<nav id="site-navigation" class="main-navigation" role="navigation">-->
		<!--			<button class="menu-toggle" aria-controls="primary-menu" aria-expanded="false">-->
		<?php /*esc_html_e( 'Primary Menu', 'fight' ); */ ?>
		<!--				</button>-->
		<?php /*wp_nav_menu( array( 'theme_location' => 'menu-1', 'menu_id' => 'primary-menu' ) ); */ ?>
		<!--		</nav><!-- #site-navigation -->
	</header><!-- #masthead -->
	

	<div id="content" class="site-content">
