<?php
/**
 * Основные параметры WordPress.
 *
 * Скрипт для создания wp-config.php использует этот файл в процессе
 * установки. Необязательно использовать веб-интерфейс, можно
 * скопировать файл в "wp-config.php" и заполнить значения вручную.
 *
 * Этот файл содержит следующие параметры:
 *
 * * Настройки MySQL
 * * Секретные ключи
 * * Префикс таблиц базы данных
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** Параметры MySQL: Эту информацию можно получить у вашего хостинг-провайдера ** //
/** Имя базы данных для WordPress */
define('DB_NAME', 'wp_ob');

/** Имя пользователя MySQL */
define('DB_USER', 'root');

/** Пароль к базе данных MySQL */
define('DB_PASSWORD', 'root');

/** Имя сервера MySQL */
define('DB_HOST', 'localhost');

/** Кодировка базы данных для создания таблиц. */
define('DB_CHARSET', 'utf8mb4');

/** Схема сопоставления. Не меняйте, если не уверены. */
define('DB_COLLATE', '');

/**#@+
 * Уникальные ключи и соли для аутентификации.
 *
 * Смените значение каждой константы на уникальную фразу.
 * Можно сгенерировать их с помощью {@link https://api.wordpress.org/secret-key/1.1/salt/ сервиса ключей на WordPress.org}
 * Можно изменить их, чтобы сделать существующие файлы cookies недействительными. Пользователям потребуется авторизоваться снова.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '[C&^>n6p4Jy{EpZmS.2@^7DPoh6q}FX77Aj?uG2I*AW[jK!~cD:b@Y%tI=ho,M)j');
define('SECURE_AUTH_KEY',  '7EVL{q>bPiGKQR`R^bq}2G2uhD2I#Q~+P|#;gTUa$@W/e<f2d),b<&}8xISc~j>i');
define('LOGGED_IN_KEY',    'G@t];:<]i.-m_Fp+#A[p78YoLHQqsb#`^y1nIR9#,jOOik59@g,K5G@#>WndWl<|');
define('NONCE_KEY',        'A-6kID4LN&[mIU),u.->5aaq8~MZdn{}dL/3qH^-Q33`Yw=Ls+TDFKNPUa.d#n~-');
define('AUTH_SALT',        'wpZEyo)l^/U%K, &WiuX2R;qZ<<!Q/TY,UlS~)c2*dMp@/kwi{X_l6[aRRjQkY5<');
define('SECURE_AUTH_SALT', '?G8zx0)}XB)h@[nLIO3*bojG+P:FjoLWmksth6O/O6$xrUQ:s :L2gy+Ch2?a8uc');
define('LOGGED_IN_SALT',   'k5>NDlFJkdFU[^N[)RXv3Jj!SQhuW{Mr6{UnbIDn7&B&_} x5z*MgO1TJZ+B.bvj');
define('NONCE_SALT',       '?Q:[g`1@T4?vs$hidw4IehJL(|G=,fg3l.#B]7uC#mrLEmdPb(c!LfuzWA 93iR?');

/**#@-*/

/**
 * Префикс таблиц в базе данных WordPress.
 *
 * Можно установить несколько сайтов в одну базу данных, если использовать
 * разные префиксы. Пожалуйста, указывайте только цифры, буквы и знак подчеркивания.
 */
$table_prefix  = 'wp_';

/**
 * Для разработчиков: Режим отладки WordPress.
 *
 * Измените это значение на true, чтобы включить отображение уведомлений при разработке.
 * Разработчикам плагинов и тем настоятельно рекомендуется использовать WP_DEBUG
 * в своём рабочем окружении.
 *
 * Информацию о других отладочных константах можно найти в Кодексе.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* Это всё, дальше не редактируем. Успехов! */

/** Абсолютный путь к директории WordPress. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Инициализирует переменные WordPress и подключает файлы. */
require_once(ABSPATH . 'wp-settings.php');
if(is_admin()) {
	add_filter('filesystem_method', create_function('$a', 'return "direct";' ));
	define( 'FS_CHMOD_DIR', 0751 );
}